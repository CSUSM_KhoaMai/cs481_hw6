﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


//This is helped by Jacob, so some of the code may look sytematically the same.
namespace hw6_json
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void DataInsertion(object sender, System.EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected) //check wifi disconnection
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Token " + "3a5bb5c9f4a8d817a752e26dc2eb2df8ed9f23b0");//API
                var uri = new Uri(string.Format($"https://owlbot.info/api/v4/dictionary/" + $"{entry_box.Text}"));//URI
                var request = new HttpRequestMessage(); //connection starts here
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                HttpResponseMessage response = await client.SendAsync(request);
                Words wordData = null;
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    wordData = Words.FromJson(content);
                    BindingContext = wordData;// binding context
                }
                else await DisplayAlert("No Results!", "Check your spelling and search again.", "OK").ConfigureAwait(false);
            } // check if word is avaiblable
            else await DisplayAlert("Oops", "Please Connect Your Device To The Internet", "OK").ConfigureAwait(false);
            
            if (entry_box.Text == "") //check if there is a string
            {
                entry_box.Placeholder = "Enter word!";
                return;
            }
        }//no connection
    }

    public partial class Words // our data object
    {
        [JsonProperty("definitions")]
        public Definitions[] Definition { get; set; }
        [JsonProperty("word")]
        public string word { get; set; }
        [JsonProperty("pronunciation")]
        public string pronunciation { get; set; }
        //converting from json to a deserialized object
        public static Words FromJson(string json) => JsonConvert.DeserializeObject<Words>(json, hw6_json.Converter.Settings);
    }

    public partial class Definitions// our array of objects of definitions data
    {
        [JsonProperty("type")]
        public string type { get; set; }
        [JsonProperty("definition")]
        public string definition { get; set; }
        [JsonProperty("example")]
        public string example { get; set; }
        [JsonProperty("image_url")]
        public object imageUrl { get; set; }
        [JsonProperty("emoji")]
        public object emoji { get; set; }
        //converting from json to a deserialized object
        public static Definitions FromJson(string json) => JsonConvert.DeserializeObject<Definitions>(json, hw6_json.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Words self) => JsonConvert.SerializeObject(self, hw6_json.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
        {
            new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
        },
        };
    }
}





